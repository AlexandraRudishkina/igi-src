package by.gsu.igi.students.MayorauAndrey.Lab1.Task2;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите первое число: ");
        int firstNumber = scanner.nextInt();
        System.out.println("Введите второе число: ");
        int secondNumber = scanner.nextInt();

        int amount = firstNumber + secondNumber;
        System.out.println(firstNumber + " + " + secondNumber + " = " + amount);

        int difference = firstNumber - secondNumber;
        System.out.println(firstNumber + " - " + secondNumber + " = " + difference);

        int multiplication = firstNumber * secondNumber;
        System.out.println(firstNumber + " * " + secondNumber + " = " + multiplication);

        if (firstNumber == 0 || secondNumber == 0) {
            System.out.println("Division by zero!");
        } else {
            int division = firstNumber / secondNumber;
            System.out.println(firstNumber + " / " + secondNumber + " = " + division);
        }

        int mod = firstNumber % secondNumber;
        System.out.println(firstNumber + " mod " + secondNumber + " = " + mod);
    }
}

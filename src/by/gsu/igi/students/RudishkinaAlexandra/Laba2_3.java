package by.gsu.igi.students.RudishkinaAlexandra;


import java.util.Scanner;

public class Laba2_3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("текстовые данные");
        System.out.print(" Радиус =");
        float radius = scanner.nextFloat();
        System.out.println("ожидаемый вывод");
        System.out.println("периметр = " + 2 * Math.PI * radius );
        System.out.println("площадь  = " + Math.PI * Math.pow(radius, 2));
    }
}

package by.gsu.igi.students.RudishkinaAlexandra;


import java.util.Scanner;

public class Laba2_2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("ведите первое число");
        int firstNumber = scanner.nextInt();
        System.out.println("введите второе число");
        int secondNumber = scanner.nextInt();
        System.out.println("Ожидаемый вывод");
        System.out.println(firstNumber + "+" + secondNumber + "=" + (firstNumber + secondNumber));
        System.out.println(firstNumber + "-" + secondNumber + "=" +(firstNumber - secondNumber));
        System.out.println(firstNumber + "*" + secondNumber + "=" + (firstNumber * secondNumber));
        System.out.println(firstNumber + "/" + secondNumber + "=" + (firstNumber / secondNumber));
        System.out.println(firstNumber + " mod " + secondNumber + "=" + (firstNumber / secondNumber));
    }
}
